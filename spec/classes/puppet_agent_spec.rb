require 'spec_helper'

describe 'puppet_agent' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      it { is_expected.to compile }
      it { is_expected.to compile.with_all_deps }

      it { is_expected.to contain_class('puppet_agent') }
      it { is_expected.to contain_class('puppet_agent::install').that_comes_before('Class[puppet_agent::config]') }
      it { is_expected.to contain_class('puppet_agent::config').that_comes_before('Class[puppet_agent::service]') }
      it { is_expected.to contain_class('puppet_agent::service') }

      context 'puppet_agent::install' do
        it { is_expected.to contain_package('puppet-agent') }
      end

      context 'puppet_agent::service' do
        it { is_expected.to contain_service('puppet').with('ensure' => 'running', 'enable' => true) }
      end
    end
  end
end
