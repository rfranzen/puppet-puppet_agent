require 'spec_helper_acceptance'

describe 'puppet_agent class' do
  context 'should work with no errors' do
    it 'when configuring puppet_agent without parameters' do
      pp = <<-EOS
        class { '::puppet_agent':
          certname    => 'test',
          environment => 'testing',
          runinterval => 1800,
          server      => 'puppetagent.localhost.local',
        }
      EOS

      apply_manifest(pp, catch_failures: true)
      wait_for_finish_on default
      configure_agent_on default
      apply_manifest(pp, catch_changes: true)
      wait_for_finish_on default
    end

    describe package('puppet-agent') do
      it { is_expected.to be_installed }
    end

    describe service('puppet') do
      it { is_expected.to be_enabled }
      it { is_expected.to be_running }
    end
  end
end
