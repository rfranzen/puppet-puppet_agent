# == Class puppet_agent
#
# Manage puppet_agent
#
#
# === Parameters
#
# [certname]
#   Certificate name to be used in puppet-agent config file
# [environment]
#   Environment name to be used in puppet-agent config file
# [runinterval]
#   Interval between agent runs
# [server]
#   Server for puppet-agent
# [config_file]
#   puppet-agent configuration file
# [collection]
#   puppet collection. puppet5, puppet6
# [package_name]
#   puppet-agent package name
# [package_version]
#   puppet-agent version
# [service_name]
#   puppet-agent service name
# [manage_repo]
#   true or false to manage puppet repository

class puppet_agent (
  $certname        = undef,
  $environment     = undef,
  $runinterval     = undef,
  $server          = undef,
  $config_file     = '/etc/puppetlabs/puppet/puppet.conf',
  $collection      = 'puppet6',
  $package_name    = 'puppet-agent',
  $package_version = 'present',
  $service_name    = 'puppet',
  $manage_repo     = true,
){

  include puppet_agent::install
  include puppet_agent::config
  include puppet_agent::service

  Class['puppet_agent::install']
  -> Class['puppet_agent::config']
    -> Class['puppet_agent::service']

}
