# == Class puppet_agent::install
#
# This class install puppet_agent
#

class puppet_agent::install {

  if $::osfamily == 'Debian' {
    $provider = 'dpkg'

    if getvar('::puppet_agent::manage_repo') == true {
      apt::source { 'puppet':
        location => 'http://apt.puppetlabs.com',
        repos    => $::puppet_agent::collection,
        key      => {
          id     => '6F6B15509CF8E59E6E469F327F438280EF8D349F',
          server => 'pgp.mit.edu',
        },
        notify   => Exec['puppet_repo_force'],
      }

      exec { 'puppet_repo_force':
        command     => "/bin/echo 'forcing apt update for puppet ${::puppet_agent::collection}'",
        refreshonly => true,
        logoutput   => true,
        subscribe   => Exec['apt_update'],
      }
    }
  }

  package { $::puppet_agent::package_name:
    ensure   => $::puppet_agent::package_version,
    provider => $provider,
  }

}
