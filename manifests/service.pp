# == Class puppet_agent::service
#
# This class manage puppet service
#

class puppet_agent::service {
  service { $::puppet_agent::service_name:
    ensure     => running,
    enable     => true,
    hasrestart => true,
    hasstatus  => true,
  }
}
