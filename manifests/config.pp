# == Class puppet_agent::config
#
# This class manage puppet config file
#

class puppet_agent::config {

  augeas { 'certname':
    context => "/files${::puppet_agent::config_file}",
    changes => [ "set main/certname ${::puppet_agent::certname}", ],
  }

  augeas { 'server':
    context => "/files${::puppet_agent::config_file}",
    changes => [ "set main/server ${::puppet_agent::server}", ],
  }

  augeas { 'environment':
    context => "/files${::puppet_agent::config_file}",
    changes => [ "set main/environment ${::puppet_agent::environment}", ],
  }

  augeas { 'runinterval':
    context => "/files${::puppet_agent::config_file}",
    changes => [ "set main/runinterval ${::puppet_agent::runinterval}", ],
  }

}
