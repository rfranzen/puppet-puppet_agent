
# puppet_agent

#### Table of Contents

1. [Overview](#overview)
2. [Supported Platforms](#platforms)
3. [Requirements](#requirements)
4. [Installation](#installation)
5. [Reference](#reference)
6. [Author / Contributors](#contribute)

## Overview

This module will manage puppet-agent 5.x and 6.x in your system.

It's a simple module that manage puppet repository, package, service and configuration file.

Augeas resource type is used to manage parameters inside the puppet.conf.

This module was based on these two modules:
* https://forge.puppet.com/gutocarvalho/puppetagent
* https://forge.puppet.com/puppetlabs/puppet_agent


## Platforms
This module was tested under these platforms:
* Debian 8 and 9 (x86_64 arch)
* Ubuntu 16.04 and 18.04 (x86_64 arch)

## Requirements
Your agents must be running a minimum version of Puppet 5.

## Installation

via git
(inside the modules directory)
~~~
git clone https://gitlab.com/rfranzen/puppet-puppet_agent.git puppet_agent
~~~

via puppet
~~~
puppet module install rfranzen/puppet_agent
~~~

via puppetfile
~~~
mod 'rfranzen-puppet_agent'
~~~

## Usage
~~~puppet
class { '::puppet_agent':
  certname        => "%{trusted.certname}",
  environment     => 'production',
  runinterval     => 1800,
  server          => 'puppetserver.mydomain.com',
  collection      => 'puppet6',
  manage_repo     => true,
}
~~~
This will ensure latest version of puppet6 available in repository. Also will ensure the puppet.conf file have the correct parameters.

## Reference
### Classes
~~~puppet
::puppet_agent
::puppet_agent::install #private
::puppet_agent::config #private
::puppet_agent::service #private
~~~

### Hiera
~~~
puppet_agent::certname: "%{trusted.certname}"
puppet:agent::environment: 'production'
puppet:agent::server: 'puppetserver.mydomain.com'
puppet:agent::collection: 'puppet6'
puppet:agent::manage_repo: false
~~~

## Contribute
Feel free to help and improve it.

Ricardo Franzen (rfranzen at gmail dot com)
